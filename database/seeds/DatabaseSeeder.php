<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Category;
use App\Brand;
use App\ProductModel;
use App\Call;
use App\Message;
use App\Product;
use App\ProductReview;
use App\UserFavourite;
use App\UserReview;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $usersQuantity = 50;
        $categoryQuantity = 30;
        $brandQuantity = 40;
        $modelQuantity = 60;
        $productQuantity = 150;
        $callQuantity = 50;
        $messageQuantity = 60;
        $productReviewQuantity = 50;
        $userFavouriteQuantity = 70;
        $userReviewQuantity = 40;

        factory(User::class, $usersQuantity)->create();
        factory(Category::class, $categoryQuantity)->create();
        factory(Brand::class, $brandQuantity)->create();
        factory(ProductModel::class, $modelQuantity)->create();
        factory(Product::class, $productQuantity)->create();
        factory(Call::class, $callQuantity)->create();
        factory(Message::class, $messageQuantity)->create();
        factory(ProductReview::class, $productReviewQuantity)->create();
        factory(UserFavourite::class, $userFavouriteQuantity)->create();
        factory(UserReview::class, $userReviewQuantity)->create();
    }
}
