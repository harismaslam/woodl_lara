<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('model_id');
            $table->integer('brand_id');
            $table->integer('category_id');
            $table->integer('user_id');
            $table->boolean('reviewed');
            $table->integer('type');
            $table->double('sell_price');
            $table->double('rent_price');
            $table->string('url');
            $table->text('location');
            $table->text('image1');
            $table->text('image2');
            $table->text('image3');
            $table->double('cord_lat');
            $table->double('cord_long');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
