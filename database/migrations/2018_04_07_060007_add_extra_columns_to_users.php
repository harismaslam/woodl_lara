<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraColumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('company')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('city')->nullable();
            $table->string('website')->nullable();
            $table->double('cord_lat')->nullable();
            $table->double('cord_long')->nullable();
            $table->text('photo')->nullable();
            $table->text('profile_pic')->nullable();
            $table->boolean('push_notify')->default(0);
            $table->string('currency_code')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->text('prof_desc')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'first_name',
                'last_name',
                'company',
                'phone',
                'mobile',
                'city',
                'website',
                'cord_lat',
                'cord_long',
                'photo',
                'profile_pic',
                'push_notify',
                'currency_code',
                'currency_symbol',
                'prof_desc'
             ]);
        });
    }
}
