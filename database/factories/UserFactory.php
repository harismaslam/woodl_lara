<?php

use Faker\Generator as Faker;
use App\User;
use App\Category;
use App\Brand;
use App\ProductModel;
use App\Product;
use App\Call;
use App\Message;
use App\ProductReview;
use App\UserFavourite;
use App\UserReview;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('123456'), // 123456
        'remember_token' => str_random(10),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'company' => $faker->company,
        'phone' => $faker->phoneNumber,
        'mobile' => $faker->phoneNumber,
        'city' => $faker->address,
        'website' => $faker->lastName,
        // 'photo' => $faker->ima,
        // 'profile_pic' => $faker->lastName,
        'push_notify' => $faker->randomElement([0,1]),
        'currency_code' => $faker->currencyCode,
        // 'currency_symbol' => $faker->,
        'prof_desc' => $faker->paragraph,
        'cord_lat' => $faker->latitude,
        'cord_long' => $faker->longitude,
    ];
});

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'reviewed' => Category::REVIEWED,
    ];
});

$factory->define(Brand::class, function (Faker $faker) {
    $category = Category::all()->random();
    return [
        'name' => $faker->word,
        'category_id' => $category->id,
        'reviewed' => Brand::REVIEWED,
    ];
});

$factory->define(ProductModel::class, function (Faker $faker) {
    $brand = Brand::all()->random();
    return [
        'name' => $faker->word,
        'category_id' => $brand->category_id,
        'brand_id' => $brand->id,
        'reviewed' => 1,
    ];
});

$factory->define(Product::class, function (Faker $faker) {
    $model = ProductModel::all()->random();
    $user = User::all()->random();
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph,
        'model_id' => $model->id,
        'brand_id' => $model->brand_id,
        'category_id' => $model->category_id,
        'user_id' => $user->id,
        'reviewed' => Product::REVIEWED,
        'type' => $type = $faker->randomElement([Product::SELL_PRODUCT, Product::RENT_PRODUCT, Product::BOTH_PRODUCT]),
        'sell_price' => $type == Product::SELL_PRODUCT || $type == Product::BOTH_PRODUCT ? $faker->randomNumber(): 0,
        'rent_price' => $type == Product::RENT_PRODUCT || $type == Product::BOTH_PRODUCT ? $faker->randomNumber(): 0,
        'url' => $faker->url,
        'image1' => '',
        'image2' => '',
        'image3' => '',
        'location' => $faker->address,
        'cord_lat' => $faker->latitude,
        'cord_long' => $faker->longitude,
    ];
});

$factory->define(Call::class, function (Faker $faker) {
    $product = Product::all()->random();
    $user = User::all()->except($product->user_id)->random();
    return [
        'user_id' => $user->id,
        'product_id' => $product->id,
        'receiver_user_id' => $product->user_id,
    ];
});

$factory->define(Message::class, function (Faker $faker) {
    $product = Product::all()->random();
    $user = User::all()->except($product->user_id)->random();
    return [
        'user_id' => $user->id,
        'product_id' => $product->id,
        'receiver_user_id' => $product->user_id,
        'message' => $faker->paragraph
    ];
});

$factory->define(ProductReview::class, function (Faker $faker) {
    $product = Product::all()->random();
    $user = User::all()->random();
    return [
        'rating' => $faker->numberBetween(0,5),
        'message' => $faker->paragraph,
        'user_id' => $user->id,
        'product_id' => $product->id,
    ];
});

$factory->define(UserFavourite::class, function (Faker $faker) {
    $product = Product::all()->random();
    $user = User::all()->random();
    return [
        'user_id' => $user->id,
        'product_id' => $product->id,
    ];
});

$factory->define(UserReview::class, function (Faker $faker) {
    $user = User::all()->random();
    $reviewUser = User::all()->except($user->id)->random();
    return [
        'rating' => $faker->numberBetween(0,5),
        'message' => $faker->paragraph,
        'user_id' => $user->id,
        'reviewed_user_id' => $reviewUser->id,
    ];
});
