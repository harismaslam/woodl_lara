<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    const REVIEWED = 1;
    const NONREVIEWED = 0;
    //Sell type only
    const SELL_PRODUCT = 1;
    //Rent type only
    const RENT_PRODUCT = 2;
    //Both sell and rent
    const BOTH_PRODUCT = 3;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'description',
        'model_id',
        'brand_id',
        'category_id',
        'user_id',
        'reviewed',
        'type',
        'sell_price',
        'rent_price',
        'url',
        'location',
        'image1',
        'image2',
        'image3',
        'cord_lat',
        'cord_long',
    ];

    public function model(){
        return $this->belongsTo('App\ProductModel');
    }
    public function brand(){
        return $this->belongsTo('App\Brand');
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function userFavourites(){
        return $this->hasMany('App\UserFavourite');
    }

    public function productReviews(){
        return $this->hasMany('App\ProductReview');
    }
}
