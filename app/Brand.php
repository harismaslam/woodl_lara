<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;

    const REVIEWED = 1;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'category_id',
        'reviewed',
    ];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function models(){
        return $this->hasMany(ProductModel::class);
    }
}
