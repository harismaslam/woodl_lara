<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductModel extends Model
{
    use SoftDeletes;
    const REVIEWED = 1;
    
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'category_id',
        'brand_id',
        'reviewed',
    ];

    public function brand(){
        return $this->belongsTo('App\Brand');
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }
}
