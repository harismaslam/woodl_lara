<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'company', 'phone', 'mobile', 'city', 'website',
        'cord_lat', 'cord_long', 'photo', 'profile_pic', 'push_notify', 'currency_code', 'currency_symbol', 'prof_desc'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function userFavourites(){
        return $this->hasMany('App\UserFavourite');
    }

    public function userReviews(){
        return $this->hasMany('App\UserReview');
    }
}
