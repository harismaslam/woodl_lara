<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'category_id'=>'required_if:other_category,',
            'brand_id'=>'required_if:other_brand,',
            'model_id'=>'required_if:other_model,',
            'sell_price'=>'required_if:type,1,3|numeric',
            'rent_price'=>'required_if:type,2,3|numeric',
            'url'=>'required|url',
            'location'=>'required',
            'image1'=>'mimes:jpeg,png,jpg,gif,svg|max:1024',
            'image2'=>'mimes:jpeg,png,jpg,gif,svg|max:1024',
            'image3'=>'mimes:jpeg,png,jpg,gif,svg|max:1024',
        ];
    }

    public function messages()
    {        
        return [
            'category_id.required' => 'The category field is required.',
            'brand_id.required' => 'The Brand field is required.',
            'model_id.required' => 'The Model field is required.',
            'location.required' => 'The Address field is required.',
        ];
    }
}
