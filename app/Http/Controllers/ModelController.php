<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductModel;
use App\Category;
use Illuminate\Support\Facades\Session;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productModels = ProductModel::all();
        
        return view('models.list', compact('productModels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id')->all();
        return view('models.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'category_id'=>'required',
            'brand_id'=>'required',
        ];
        $messages = [
            'category_id.required' => 'The category field is required.',
            'brand_id.required' => 'The brand field is required.',
        ];
        $request->validate($rules, $messages);

        $data = $request->all();
        $data['reviewed'] = ProductModel::REVIEWED;

        ProductModel::create($data);
        Session::flash('success', 'Model created successfully.');
        return redirect('models');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductModel $model)
    {
        $categories = Category::pluck('name', 'id')->all();
        $brands = Category::find($model->category_id)->brands->pluck('name', 'id')->all();
        return view('models.edit', compact('model','brands', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductModel $model)
    {
        $rules = [
            'name' => 'required',
            'category_id'=>'required',
            'brand_id'=>'required',
        ];
        $messages = [
            'category_id.required' => 'The category field is required.',
            'brand_id.required' => 'The brand field is required.',
        ];
        $request->validate($rules, $messages);
        
        $model->update($request->all());

        if ($model->wasChanged()) {
            Session::flash('success', 'Model updated successfully.');
        }
        return redirect('models');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductModel $model)
    {
        $model->delete();
        Session::flash('success', 'Model deleted successfully.');
        return redirect('models');
    }
}
