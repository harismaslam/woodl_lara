<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Auth;
use App\Message;
use Illuminate\Http\Request;
use App\Call;

class MessageController extends ApiController
{
    public function send_email(Request $request)
    {
        $rules = [
            'receiver_user_id'=>'required|exists:users,id',
            'prod_id'=>'required|exists:products,id',
            'message'=>'required'
        ];
        $messages = [
            'receiver_user_id.required' => 'The receiver is required.',
            'receiver_user_id.exists' => 'The selected receiver is invalid.',
            'prod_id.required' => 'The product is required.',
            'prod_id.exists' => 'The selected product is invalid.',
        ];

        $request->validate($rules, $messages);

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $data['product_id'] = $request->prod_id;

        Message::create($data);
        
        return $this->showMessage('Message send successfully.', 200);
    }

    public function make_call(Request $request)
    {
        $rules = [
            'receiver_user_id'=>'required|exists:users,id',
            'prod_id'=>'required|exists:products,id',
        ];
        $messages = [
            'receiver_user_id.required' => 'The receiver is required.',
            'receiver_user_id.exists' => 'The selected receiver is invalid.',
            'prod_id.required' => 'The product is required.',
            'prod_id.exists' => 'The selected product is invalid.',
        ];

        $request->validate($rules, $messages);

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $data['product_id'] = $request->prod_id;

        Call::create($data);
        
        return $this->showMessage('Placed call successfully.', 200);
    }
}
