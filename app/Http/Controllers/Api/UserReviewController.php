<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;
use App\UserReview;

class UserReviewController extends ApiController
{
    use ApiResponser;

    public function addReview(Request $request){

        $rules = [
            'user_id'=>'required|exists:users,id',
            'message' => 'required|string',
            'rating' => 'required|integer|between:0,5'
        ];
        $messages = [
            'user_id.required' => 'The user field is required.',
            'user_id.exists' => 'The selected user is invalid.',
        ];
        $request->validate($rules, $messages);

        $reviewed = UserReview::where(['reviewed_user_id' => Auth::user()->id, 'user_id'=> $request->user_id])->first();
        if(!empty($reviewed)){
            return $this->errorResponse('Review already added for this user', 409);
        }

        $data = $request->all();
        $data['reviewed_user_id'] = Auth::user()->id;

        UserReview::create($data);
        return $this->showMessage('Review added successfully.', 201);
    }
}
