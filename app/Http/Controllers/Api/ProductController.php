<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\StoreProductRequest;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\Category;
use App\Brand;
use App\ProductModel;

class ProductController extends ApiController
{
    public function store(StoreProductRequest $request)
    {
        $data = $request->all();
        $user = Auth::user();

        if ($file = $request->file('image1')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['image1'] = $name;
        }
        if ($file = $request->file('image2')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['image2'] = $name;
        }
        if ($file = $request->file('image3')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['image3'] = $name;
        }
        $data['reviewed'] = Product::REVIEWED;
        if ($request->input('type')==1) {
            $data['rent_price'] = 0;
        } elseif ($request->input('type')==2) {
            $data['sell_price'] = 0;
        }

        if ($request->input('other_category')) {
            $category = Category::create(['name'=>$request->input('other_category'), 'reviewed'=>0]);
            $data['reviewed'] = Product::NONREVIEWED;
            $data['category_id'] = $category->id;
        }
        if ($request->input('other_brand')) {
            $brand = Brand::create(['name'=>$request->input('other_brand'), 'reviewed'=>0, 'category_id' => $category->id]);
            $data['reviewed'] = Product::NONREVIEWED;
            $data['brand_id'] = $brand->id;
        }
        if ($request->input('other_model')) {
            $model = ProductModel::create(
                ['name'=>$request->input('other_model'),
                'reviewed'=>0,
                 'category_id' => $category->id,
                 'brand_id' => $brand->id
                 ]
            );
            $data['reviewed'] = Product::NONREVIEWED;
            $data['model_id'] = $model->id;
        }

        $data['cord_lat'] = '55.707853';
        $data['cord_long'] = '12.525717';

        $user->products()->create($data);
        return $this->showMessage('Product created successfully.', 201);
    }
}
