<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\AddUserRequest;
use App\User;

class SignupController extends ApiController
{
    public function newUser(AddUserRequest $request)
    {
        $data = $request->all();

        if ($file = $request->file('photo')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['photo'] = $name;
        }

        $data['password'] = bcrypt($request->password);

        
        $user = User::create($data);

        return $this->showOne($user, 201);
    }
}
