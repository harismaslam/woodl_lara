<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Category;
use App\Brand;
use App\ProductModel;
use App\Product;

class HomeController extends ApiController
{
    public function splash()
    {
        $categories = Category::all();


        $brandArr = [];
        foreach ($categories as $category) {
            $brands = Brand::where('category_id', $category->id)->get();
            foreach ($brands as $brand) {
                $brand['count'] = Product::where('brand_id', $brand->id)->count();
            }
            $category['count'] = Product::where('category_id', $category->id)->count();

            $brandArr[$category->id] = $brands;
        }
        foreach (Brand::all() as $brand) {
            $models = ProductModel::where('brand_id', $brand->id)->get();
            foreach ($models as $model) {
                $model['count'] = Product::where('model_id', $model->id)->count();
            }
            $modelArr[$brand->id] = $models;
        }

        $tc = "Welcome to our website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern [business name]'s relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website."
        . "The term '[business name]' or 'us' or 'we' refers to the owner of the website whose registered office is [address]. Our company registration number is [company registration number and place of registration]. The term 'you' refers to the user or viewer of our website.";

        $result = collect(['category'=>$categories, 'brand' => $brandArr, 'model'=> $modelArr, 'tc' => $tc]);
        return $this->showAll($result);
    }
}
