<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class UserController extends ApiController
{
    use SendsPasswordResetEmails;

    public function getProfile()
    {
        $user = Auth::user();
        return $this->showOne($user, 200);
    }

    public function getProducts()
    {
        $products = Auth::user()->products()->where('reviewed', 1)->with('productReviews')->get();
        return $this->showAll($products);
    }

    public function saveProfile(Request $request)
    {
        $rules = [
            'name'=>'sometimes|required|string',
            'city'=>'sometimes|required|string',
            'cord_lat'=>'sometimes|required',
            'cord_long'=>'sometimes|required',
        ];
        $request->validate($rules);

        $user = Auth::user();
        $data = $request->all();
        $user->update($data);

        if ($user->wasChanged()) {
            return $this->showMessage('User updated successfully.');
        } else {
            return $this->errorResponse('You need to specify a different value to update', 422);
        }
    }

    public function changePassword(Request $request)
    {
        $rules = [
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation'=>'required',
        ];
        $request->validate($rules);
        
        $data['password'] = bcrypt($request->password);
        $user = Auth::user();

        $user->update($data);

        return $this->showMessage('Password changed successfully.');
    }

    public function userProfile(Request $request){
        $rules = [
            'profile_id' => 'required|integer',
        ];
        $request->validate($rules);

        $profile = User::find($request->profile_id);

        $products = $profile->products;

        $reviews = $profile->userReviews;

        return $this->showOne($profile);
    }

    public function forgotPassword(Request $request){
        $rules = [
            'email' => 'required|email|exists:users,email',
        ];
        $request->validate($rules);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $this->showMessage('Reset link sent to your email.');
    }
}
