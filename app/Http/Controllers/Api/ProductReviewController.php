<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Traits\ApiResponser;
use App\ProductReview;
use Illuminate\Support\Facades\Auth;

class ProductReviewController extends ApiController
{
    use ApiResponser;

    public function addReview(Request $request){

        $rules = [
            'product_id'=>'required|exists:products,id',
            'message' => 'required|string',
            'rating' => 'required|integer|between:0,5'
        ];
        $messages = [
            'product_id.required' => 'The product field is required.',
            'product_id.exists' => 'The selected product is invalid.',
        ];
        $request->validate($rules, $messages);

        $reviewed = ProductReview::where(['user_id' => Auth::user()->id, 'product_id'=> $request->product_id])->first();
        if(!empty($reviewed)){
            return $this->errorResponse('Review already added for this product', 409);
        }

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        ProductReview::create($data);
        return $this->showMessage('Review added successfully.', 201);
    }
}
