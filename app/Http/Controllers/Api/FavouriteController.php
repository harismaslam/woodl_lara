<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Auth;
use App\UserFavourite;
use Illuminate\Http\Request;

class FavouriteController extends ApiController
{
    public function getFavourites()
    {
        //eagerloading loads first one query less
        // $favourites = UserFavourite::where('user_id', Auth::user()->id)->with(['product', 'user'])->get();
        $favourites = UserFavourite::where('user_id', Auth::user()->id)->with('product')->get();
        // lazy loading
        // $favourites = Auth::user()->userFavourites()->get();
        // if(!empty($favourites)){
        //     foreach($favourites as $favourite){
        //         $favourite->product;
        //         $favourite->user;
        //     }
        // }
        return $this->showAll($favourites);
    }

    public function addFavourites(Request $request)
    {
        $rules = [
            'product_id'=>'required|exists:products,id'
        ];
        $messages = [
            'product_id.required' => 'The product field is required.',
            'product_id.exists' => 'The selected product is invalid.',
        ];
        $request->validate($rules, $messages);

        $favourite = UserFavourite::where(['user_id' => Auth::user()->id, 'product_id'=> $request->product_id])->first();
        if(!empty($favourite)){
            return $this->errorResponse('This product is already in favourites list', 409);
        }

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        UserFavourite::create($data);
        return $this->showMessage('Favourite saved successfully.', 201);
    }

    public function deleteFavourites(Request $request)
    {
        $rules = [
            'fav_id'=>'required|exists:user_favourites,id'
        ];
        $messages = [
            'fav_id.required' => 'The favourites field is required.',
            'fav_id.exists' => 'The selected favourites is invalid.',
        ];

        $request->validate($rules, $messages);

        $favourite = UserFavourite::where(['user_id' => Auth::user()->id, 'id'=> $request->fav_id])->first();
        if(empty($favourite)){
            return $this->errorResponse('This product is not in user\'s favourites list', 409);
        }

        $favourite->delete();
        return $this->showMessage('Favourite removed successfully.', 200);
    }
}
