<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = [];
        // $users = User::whereHas('roles' , function($query) {
        //     $query->where('name', '!=', 'admin');
        // })->get();
        $users = User::whereDoesntHave('roles')->get();
        return view('users.list', compact('users'));
    }

    public function create(User $user)
    {
        return view('users.add', compact('user'));
    }

    public function store(AddUserRequest $request)
    {
        $data = $request->all();

        if ($file = $request->file('photo')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['photo'] = $name;
        }

        $data['password'] = bcrypt($request->password);

        
        User::create($data);

        // if ($request->wantsJson()) {
        //     return $this->showOne($user, 201);
        // } else {
            Session::flash('success', 'User created successfully.');
            return redirect('users');
        // }
    }

    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $data = $request->all();


        if ($request->filled('password')) {
            $data['password'] = bcrypt($request->password);
        } else {
            $data = $request->except('password');
        }

        if ($file = $request->file('photo')) {
            if (!empty($user->photo) && file_exists(public_path('images/') . $user->photo)) {
                unlink(public_path('images/') . $user->photo);
            }
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['photo'] = $name;
        }

        $user->update($data);

        if ($user->wasChanged()) {
            Session::flash('success', 'User updated successfully.');
        }
        return redirect('users');
    }

    public function destroy(User $user)
    {
        if (!empty($user->photo) && file_exists(public_path('images/') . $user->photo)) {
            unlink(public_path('images/') . $user->photo);
        }
        $user->delete();
        Session::flash('success', 'User deleted successfully.');
        return redirect('users');
    }
}
