<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests\StoreProductRequest;
use Illuminate\Support\Facades\Auth;
use App\Brand;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.list', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id')->all();
        return view('products.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $data = $request->all();
        $user = Auth::user();

        if ($file = $request->file('image1')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['image1'] = $name;
        }
        if ($file = $request->file('image2')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['image2'] = $name;
        }
        if ($file = $request->file('image3')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['image3'] = $name;
        }
        $data['reviewed'] = Product::REVIEWED;

        $data['cord_lat'] = '55.707853';
        $data['cord_long'] = '12.525717';

        $user->products()->create($data);
        Session::flash('success', 'Product created successfully.');
        return redirect('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::pluck('name', 'id')->all();
        $brands = Category::find($product->category_id)->brands->pluck('name', 'id')->all();
        $models = Brand::find($product->brand_id)->models->pluck('name', 'id')->all();
        return view('products.edit', compact('product','models','brands', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $data = $request->all();

        if ($file = $request->file('image1')) {
            if (!empty($product->image1) && file_exists(public_path('images/') . $product->image1)) {
                unlink(public_path('images/') . $product->image1);
            }
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['image1'] = $name;
        }
        if ($file = $request->file('image2')) {
            if (!empty($product->image2) && file_exists(public_path('images/') . $product->image2)) {
                unlink(public_path('images/') . $product->image2);
            }
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['image2'] = $name;
        }
        if ($file = $request->file('image3')) {
            if (!empty($product->image3) && file_exists(public_path('images/') . $product->image3)) {
                unlink(public_path('images/') . $product->image3);
            }
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $data['image3'] = $name;
        }

        $product->update($data);

        if ($product->wasChanged()) {
            Session::flash('success', 'Product updated successfully.');
        }
        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if (!empty($product->image1) && file_exists(public_path('images/') . $product->image1)) {
            unlink(public_path('images/') . $product->image1);
        }
        if (!empty($product->image2) && file_exists(public_path('images/') . $product->image2)) {
            unlink(public_path('images/') . $product->image2);
        }
        if (!empty($product->image3) && file_exists(public_path('images/') . $product->image3)) {
            unlink(public_path('images/') . $product->image3);
        }
        $product->delete();
        Session::flash('success', 'Product deleted successfully.');
        return redirect('products');
    }
}
