<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Session;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        return view('brands.list', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id')->all();
        return view('brands.add', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'category_id'=>'required'
        ];
        $messages = [
            'category_id.required' => 'The category field is required.',
        ];
        $request->validate($rules, $messages);

        $data = $request->all();
        $data['reviewed'] = Brand::REVIEWED;

        Brand::create($data);
        Session::flash('success', 'Brand created successfully.');
        return redirect('brands');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        $categories = Category::pluck('name', 'id')->all();
        return view('brands.edit', compact('brand', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $rules = [
            'name' => 'required',
            'category_id'=>'required'
        ];
        $messages = [
            'category_id.required' => 'The category field is required.',
        ];
        $request->validate($rules, $messages);
        
        $brand->update($request->all());

        if ($brand->wasChanged()) {
            Session::flash('success', 'Brand updated successfully.');
        }
        return redirect('brands');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        $brand->delete();
        Session::flash('success', 'Brand deleted successfully.');
        return redirect('brands');
    }

    public function getBrandModels(Request $request){
        $brandId = $request->get('brand_id');
        $models = Brand::find($brandId)->models;
        return response()->json($models);
    }
}
