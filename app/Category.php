<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    const REVIEWED = 1;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'reviewed',
    ];
    
    public function brands(){
        return $this->hasMany(Brand::class);
    }
}
