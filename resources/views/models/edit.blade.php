@extends('layouts.main') 
@section('title', 'Edit Brand') 
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                {!!Form::open(['method'=>'DELETE', 'route'=>['models.destroy', $model->id], 'class' => 'form-delete'])!!} {!! Form::submit('Delete',
                ['class'=>'btn btn-danger pull-right']) !!} {!! Form::close() !!}
            </div>
            <div class="box-body">
                <div>
    @include('includes.form_error')
                </div>
                {!! Form::model($model, ['method'=>'PATCH', 'route'=>['models.update', $model->id]]) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('category_id', 'Category') !!} {!! Form::select('category_id', [''=>'Choose an Option']+$categories, null,
                    ['class'=>'form-control', 'id'=>'category']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('brand_id', 'Brand') !!} {!! Form::select('brand_id', [''=>'Choose an Option']+$brands, null, ['class'=>'form-control', 'id'=>'brand'])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Update Model', ['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function () {
jQuery('#category').change(function () {
    var _token = $("input[name='_token']").val();
    var cat_id = jQuery(this).val();
    jQuery.ajax({
        type: 'POST',
        url: "{{route('getCategoryBrands')}}",
        data: {
            _token:_token,
            cat_id: cat_id
        },
        success: function (data) {
            jQuery('#brand').empty();
            var brand = data;
            for (var i = 0; i < brand.length; i++) {
                jQuery('#brand').append('<option id=' + brand[i].id + ' value=' + brand[i].id + '>' + brand[i].name + '</option>');
            }
        }
    });
});
});

</script>
@endsection