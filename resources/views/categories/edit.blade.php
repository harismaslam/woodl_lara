@extends('layouts.main') 
@section('title', 'Edit Category') 
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                {!!Form::open(['method'=>'DELETE', 'route'=>['categories.destroy', $category->id], 'class' => 'form-delete'])!!} {!! Form::submit('Delete Category',
                ['class'=>'btn btn-danger pull-right']) !!} {!! Form::close() !!}
            </div>
            <div class="box-body">
                <div>
    @include('includes.form_error')
                </div>
                {!! Form::model($category, ['method'=>'PATCH', 'route'=>['categories.update', $category->id]]) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Update Category', ['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection