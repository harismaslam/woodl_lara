@extends('layouts.main') 
@section('title', 'Add Category') 
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-body">
                <div>
    @include('includes.form_error')
                </div>
                {!! Form::open(['method'=>'POST', 'route'=>['categories.store']]) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Add Category', ['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection