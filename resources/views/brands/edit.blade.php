@extends('layouts.main') 
@section('title', 'Edit Brand') 
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                {!!Form::open(['method'=>'DELETE', 'route'=>['brands.destroy', $brand->id], 'class' => 'form-delete'])!!} {!! Form::submit('Delete',
                ['class'=>'btn btn-danger pull-right']) !!} {!! Form::close() !!}
            </div>
            <div class="box-body">
                <div>
    @include('includes.form_error')
                </div>
                {!! Form::model($brand, ['method'=>'PATCH', 'route'=>['brands.update', $brand->id]]) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('category_id', 'Category') !!} {!! Form::select('category_id', [''=>'Choose an Option']+$categories, null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Update Brand', ['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection