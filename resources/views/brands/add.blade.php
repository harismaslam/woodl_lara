@extends('layouts.main') 
@section('title', 'Add Brand') 
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-body">
                <div>
    @include('includes.form_error')
                </div>
                {!! Form::open(['method'=>'POST', 'route'=>['brands.store']]) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('category_id', 'Category') !!} {!! Form::select('category_id', [''=>'Choose an Option']+$categories, null,
                    ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Add Brand', ['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection