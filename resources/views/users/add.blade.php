@extends('layouts.main') 
@section('title', 'Add User') 
@section('content')
<div class="row">
    <div class="col-lg-12">
        <!-- general form elements -->
        <div class="box box-primary">
            {{--
            <div class="box-header with-border">
                <h3 class="box-title">Add new product by using the form below</h3>
            </div> --}}
            <!-- /.box-header -->
            <div class="box-body">
                <div>
    @include('includes.form_error')
                </div>
                {!! Form::open(['method'=>'POST', 'route'=>['users.store'], 'files'=>true]) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Enter name'])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email') !!} {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Enter email'])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Password') !!} {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Enter password'])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Confirm Password') !!} {!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Confirm password'])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('mobile', 'Mobile') !!} {!! Form::text('mobile', null, ['class'=>'form-control', 'placeholder'=>'Enter mobile'])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('city', 'Address') !!} {!! Form::text('city', null, ['class'=>'form-control', 'placeholder'=>'Enter address'])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('photo', 'Profile Picture') !!} {!! Form::file('photo') !!}
                    <p class="help-block">Upload profile picture</p>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {{-- </form> --}}
        </div>

    </div>
</div>
@endsection
 
@section('scripts')
<script>
    $(function () {
});

</script>
@endsection