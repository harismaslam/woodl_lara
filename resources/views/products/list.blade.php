@extends('layouts.main') 
@section('title', 'Products') 
@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset("/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css")}}">
@endsection
 
@section('scripts')
<!-- DataTables -->
<script src="{{ asset("/bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
<script src="{{ asset("/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>

<script>
    $(function () {
          $('#example1').DataTable()
    });
</script>
@endsection
 
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <a href="{{ route('products.create') }}" class="btn btn-default btn-info pull-right clearfix"> Add Product </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div></div>
                <div>
    @include('includes.form_success')
                </div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Model</th>
                            <th>Brand</th>
                            <th>Category</th>
                            <th>Created</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($products) @foreach($products as $product)
                        <tr>
                            <td>{{ $product->name}}</td>
                            <td>{{ $product->model? $product->model->name : ''}}</td>
                            <td>{{ $product->brand? $product->brand->name : ''}}</td>
                            <td>{{ $product->category? $product->category->name : ''}}</td>
                            <td>
                                @if($product->created_at) {{$product->created_at->diffForHumans()}} @endif
                            </td>
                            <td>
                                <a href="{{ route('products.edit', $product->id) }}"><i class="fa fa-fw fa-edit"></i></a>
                            </td>
                        </tr>
                        @endforeach @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Model</th>
                            <th>Brand</th>
                            <th>Category</th>
                            <th>Created</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
@endsection