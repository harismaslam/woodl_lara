@extends('layouts.main') 
@section('title', 'Add Product') 
@section('content')
<div class="row">
    <div class="col-lg-12">
        <!-- general form elements -->
        <div class="box box-primary">
            {{--
            <div class="box-header with-border">
                <h3 class="box-title">Add new product by using the form below</h3>
            </div> --}}
            <!-- /.box-header -->
            <div class="box-body">
                <div>
    @include('includes.form_error')
                </div>
                {!! Form::open(['method'=>'POST', 'route'=>['products.store'], 'files'=>true]) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Enter name'])
                            !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('description', 'Description') !!} {!! Form::textarea('description', null, ['class'=>'form-control', 'placeholder'=>'Enter description', 'size' => '30x3']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('location', 'Address') !!} {!! Form::text('location', null, ['class'=>'form-control', 'placeholder'=>'Enter address']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('category_id', 'Category') !!} {!! Form::select('category_id', [''=>'Choose an Option']+$categories, null,
                            ['class'=>'form-control', 'id'=>'category']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('brand_id', 'Brand') !!} {!! Form::select('brand_id', [''=>'Choose an Option'], null, ['class'=>'form-control',
                            'id'=>'brand']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('model_id', 'Model') !!} {!! Form::select('model_id', [''=>'Choose an Option'], null, ['class'=>'form-control',
                            'id'=>'model']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('url', 'Url') !!} {!! Form::text('url', null, ['class'=>'form-control', 'placeholder'=>'Enter url']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Product Type</label>
                            <div class="radio">
                                <label>
                                    {!! Form::radio('type', '1') !!}
                                    Buy
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    {!! Form::radio('type', '2') !!}
                                    Rent Out
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    {!! Form::radio('type', '3', true) !!}
                                    Both
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6"  id="sell_wrap">
                        <div class="form-group">
                            {!! Form::label('sell_price', 'Selling Price') !!} {!! Form::text('sell_price', null, ['class'=>'form-control', 'placeholder'=>'Enter selling price']) !!}
                        </div>
                    </div>
                    <div class="col-md-6"  id="rent_wrap">
                        <div class="form-group">
                            {!! Form::label('rent_price', 'Rent per day') !!} {!! Form::text('rent_price', null, ['class'=>'form-control', 'placeholder'=>'Enter rent per day']) !!}
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('image1', 'Image 1') !!}
                            {!! Form::file('image1') !!}
                            <p class="help-block">Upload product image 1</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                                {!! Form::label('image2', 'Image 2') !!}
                                {!! Form::file('image2') !!}
                            <p class="help-block">Upload product image 2</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                                {!! Form::label('image3', 'Image 3') !!}
                                {!! Form::file('image3') !!}
                            <p class="help-block">Upload product image 3</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {{-- </form> --}}
        </div>

    </div>
</div>
@endsection

@section('scripts')
<script>
$(function () {
    jQuery('#category').trigger('change');
    jQuery('#brand').trigger('change');
    jQuery('#category').change(function () {
        var _token = $("input[name='_token']").val();
        var cat_id = jQuery(this).val();
        jQuery.ajax({
            type: 'POST',
            url: "{{route('getCategoryBrands')}}",
            data: {
                _token:_token,
                cat_id: cat_id
            },
            success: function (data) {
                jQuery('#brand').empty();
                var brand = data;
                for (var i = 0; i < brand.length; i++) {
                    jQuery('#brand').append('<option id=' + brand[i].id + ' value=' + brand[i].id + '>' + brand[i].name + '</option>');
                }
                jQuery('#brand').trigger('change');
            }
        });
    });
    jQuery('#brand').change(function () {
        var _token = $("input[name='_token']").val();
        var brand_id = jQuery(this).val();
        jQuery.ajax({
            type: 'POST',
            url: "{{route('getBrandModels')}}",
            data: {
                _token:_token,
                brand_id: brand_id
            },
            success: function (data) {
                jQuery('#model').empty();
                var model = data;
                for (var i = 0; i < model.length; i++) {
                    jQuery('#model').append('<option id=' + model[i].id + ' value=' + model[i].id + '>' + model[i].name + '</option>');
                }
            }
        });
    });
});
jQuery('input[name="type"]').change(function () {
    if (jQuery(this).val() == 1) {
        jQuery('#sell_wrap').removeClass('hide');
        jQuery('#rent_wrap').addClass('hide');
    } else if(jQuery(this).val() == 2){
        jQuery('#rent_wrap').removeClass('hide');
        jQuery('#sell_wrap').addClass('hide');
    }else if(jQuery(this).val() == 3){
        jQuery('#sell_wrap').removeClass('hide');
        jQuery('#rent_wrap').removeClass('hide');
    }
});
</script>
@endsection