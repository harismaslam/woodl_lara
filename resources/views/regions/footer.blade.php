<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Made with <i class="far fa fa-heart"></i>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="#">HMA</a>.</strong> All rights reserved.
  </footer>
