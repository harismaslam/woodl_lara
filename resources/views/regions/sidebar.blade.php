<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

  <!-- Sidebar user panel (optional) -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="{{asset('bower_components/admin-lte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
      <p>Alexander Pierce</p>
      <!-- Status -->
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">Menu</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="active"><a href="#"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
    <li class="treeview">
      <a href="#"><i class="fa fa-copyright"></i> <span>Category</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('categories.index') }}">View</a></li>
        <li><a href="{{ route('categories.create') }}">Add</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="fa fa-bitcoin"></i> <span>Brand</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('brands.index') }}">View</a></li>
        <li><a href="{{ route('brands.create') }}">Add</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="fa fa-maxcdn"></i> <span>Model</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('models.index') }}">View</a></li>
        <li><a href="{{ route('models.create') }}">Add</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="fa fa-camera"></i> <span>Product</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('products.index') }}">View</a></li>
        <li><a href="{{ route('products.create') }}">Add</a></li>
        <li><a href="#">Non Reviewed Products</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#"><i class="fa fa-address-card"></i> <span>User</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('users.index') }}">View</a></li>
        <li><a href="{{ route('users.create') }}">Add</a></li>
      </ul>
    </li>
    <li><a href="#"><i class="fa fa-trash"></i> <span>Deleted Accounts</span></a></li>
  </ul>
  <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>