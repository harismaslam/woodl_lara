<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// The authentication flow is not fired when you don't mention the auth middleware
Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/user/account', 'Api\UserController@getProfile')->name('getProfile');
    Route::post('/user/save_profile', 'Api\UserController@saveProfile')->name('saveProfile');
    Route::post('/user/change_password', 'Api\UserController@changePassword')->name('changePassword');
    Route::post('/user/profile', 'Api\UserController@userProfile')->name('userProfile');
    Route::get('/product/users', 'Api\UserController@getProducts')->name('userProdcuts');
    Route::post('/product/add', 'Api\ProductController@store')->name('product_add');
    Route::get('/user/favourites', 'Api\FavouriteController@getFavourites')->name('user_favourites');
    Route::post('/add/favourites', 'Api\FavouriteController@addFavourites')->name('add_favourites');
    Route::delete('/delete/favourites', 'Api\FavouriteController@deleteFavourites')->name('delete_favourites');
    Route::post('/user/send_mail', 'Api\MessageController@send_email')->name('send_messages');
    Route::post('/user/make_call', 'Api\MessageController@make_call')->name('make_calls');
    Route::post('/product/review/add', 'Api\ProductReviewController@addReview')->name('add_product_review');
    Route::post('/user/review/add', 'Api\UserReviewController@addReview')->name('add_user_review');
});
//forgot password
Route::post('/user/forgot-password', 'Api\UserController@forgotPassword')->name('forgotPassword');

Route::get('/splash', 'Api\HomeController@splash')->name('splash');

// Route::post('/signup', 'UserController@store')->name('signup');
Route::post('/signup', 'Api\SignupController@newUser')->name('signup');