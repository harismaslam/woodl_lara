<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    /*
     *Categories
     */
    Route::resource('categories', 'CategoryController', ['except' => ['show',]]);
    //ajax get brands of a category
    Route::post('get-category-brands', 'CategoryController@getCategoryBrands')->name('getCategoryBrands');
    
    Route::resource('brands', 'BrandController', ['except' => ['show',]]);
    //ajax get models of a brand
    Route::post('get-brand-models', 'BrandController@getBrandModels')->name('getBrandModels');
    
    Route::resource('models', 'ModelController', ['except' => ['show',]]);
    Route::resource('products', 'ProductController', ['except' => ['show',]]);
    Route::resource('users', 'UserController', ['except' => ['show',]]);
});
